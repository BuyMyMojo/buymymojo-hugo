+++
title = "About me"
slug = "about"
description = "Why you'd want to hang out with me"
path = "about"
tags = ["rust", "web"]
date = "2021-12-09"
updated = "2022-11-29"
weight = 0

[taxonomies]
tags = ["about"]
+++

My name is Owen Quinlan or more known as BuyMyMojo online.

I am a jr software engineer with many hobys that can be pretty unrelated. This website is for me to explore and write about them.
